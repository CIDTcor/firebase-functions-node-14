let functions = require('firebase-functions');
let admin = require('firebase-admin');

//admin.initializeApp(functions.config().firebase);

var serviceAccount = require("../intelihogardb-firebase-adminsdk-25d8j-2fd7164ae9");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://intelihogardb.firebaseio.com"
});
//admin.database().useEmulator("192.168.137.19", 8080);
//////////////////////////  S E N D  P U S H  N O T I F I C A T I O N S  /////////////////////////////////
let userLog = [];
exports.sendPush = functions.database.ref('/locations/{location}/notifications/{deviceNotification}').onWrite((change, context) => {
    let locationActivated = context.params.location;
    let deviceNotification = context.params.deviceNotification;
    let logsData = change.after.val();
    console.log(logsData);
    let deviceType = logsData.type;
    let deviceDate = logsData.date;
    let deviceId = logsData.id;
    let displayName = logsData.name;
    let deviceValue = logsData.value;
    let deviceStatus = logsData.status;

    return Promise.all([getUserPreferences(deviceId),getDeviceAlias(locationActivated, deviceId),loadUsers(locationActivated), getLocationName(locationActivated)])
        .then(response => {
            let resultsArray = response;
            let userPreferences = resultsArray[0];
            let deviceAlias = resultsArray[1];
            let tokensAndUsers = resultsArray[2];
            let locationName = resultsArray[3];
            let tokens = tokensAndUsers[0];
            let users = tokensAndUsers[1];
            let payload = {
                "notification": {
                    "title": getTitle(deviceType, displayName, deviceStatus) + " - " + deviceAlias + " - " + "roomName",
                    "body": locationName,
                    "sound": 'default',
                    "surveyID": "ewtawgreg-gragrag-rgarhthgbad",
                    "notId": "10"
                }
            };
            console.log(userPreferences);
            let notificationTokens = getNotificationTokens(deviceType, tokens, users, userPreferences);
            console.log('Notification Tokens: '+notificationTokens);
            // admin.messaging().sendToDevice("dqAn2AjgQVy38hwn6rliJS:APA91bHaOU2NvGl7gdX2tt7efnjThT84rEopNrgOZ9c5ueKwtD7AG6RPQBG1aRbIIbuYeorB6mbuTPDQWr89EIagzLGpxD0m8e6Y_78gYbYkdWjzcEXjCTa_GWeBaXud4ZFAPWM-SNrT", payload);
            admin.messaging().sendToDevice("ePb4PJfnSmC4cuM0gKEm_b:APA91bEWk6FkVPIafyx6NAqP9MkwX4GTLCm6W3ZkA1cmnP-KuSXRy2aYC-TfdpzA_QgeW9A7u545VEADzF6uZqiSEiejIHmYyrS2dsN8_BqN3dO31zSFxmf5O8nAX98qCPWSS04RMEba", payload);
        })
});

function getUserPreferences(deviceId) {
    const ref = admin.database().ref('/locationsUsers');
    let results = [];
    let locationIds = "";
    let userIds = "";
    let defer = new Promise((resolve, reject) => {
        ref.once('value', (snap) => {
            let data = snap.val();
            for (var property in data) {
                if (data[property].hasOwnProperty('preferences')) {
                    if ((data[property].preferences).includes(deviceId)) {
                        console.log('INCLUYE');
                        locationIds += data[property].locationId;
                        userIds += data[property].userId;
                    }
                }
            }
            results.push(locationIds);
            results.push(userIds);
            resolve(results);
        }, (err) => {
            console.log(`USER PREFERENCES ERROR: ${err}`);
            reject(err);
        });
    });
    return defer;
}

function getDeviceAlias(locationActivated, deviceId) {
    let ref = admin.database().ref(`/locations/${locationActivated}/devices/${deviceId}/alias`);
    let defer = new Promise((resolve, reject) => {
        ref.once('value', resp => {
            resolve(resp.val());
        }, (err) => {
            reject(err);
        });
    });
    return defer;
}

function loadUsers(locationActivated) {
    let dbRef = admin.database().ref(`/locations/${locationActivated}/deviceTokens`);
    let defer = new Promise((resolve, reject) => {
        dbRef.once('value', (snap) => {
            let data = snap.val();
            let tokens = {};
            let users = {};
            let userAndTokens = [];
            let deviceTokens = snapshotToArray(snap);
            for (device in deviceTokens) {
                tokens[device] = deviceTokens[device].pushToken;
                users[device] = deviceTokens[device].user;
            }
            userAndTokens.push(tokens);
            userAndTokens.push(users);
            resolve(userAndTokens);
        }, (err) => {
            console.log(`LOAD USERS ERROR: ${err}`);
            reject(err);
        });
    });
    return defer;
}

function getLocationName(locationActivated) {
    let ref = admin.database().ref(`/locations/${locationActivated}/locationInfo/locationName`);
    let defer = new Promise((resolve, reject) => {
        ref.once('value', resp => {
            let data = resp.val();
            resolve(data);
        }, (err) =>{
            console.log (err)
            reject(err);
        });
    });
    return defer;
}

function snapshotToArray(snapshot) {
    let array = [];
    snapshot.forEach(element => {
        array.push(element.val());
    });
    return array;
}

function getNotificationTokens(deviceType, tokens, users, userPreferences) {
    let notificationTokens = [];
    for (user in users) {
        if (deviceType == "alarmaIntrusos" || deviceType == "alarmaGas" || deviceType == "alarmaAgua" || deviceType == "alarmaIncendio" || deviceType == "simulacionPresencia" || deviceType == "battery") {
            notificationTokens.push(tokens[user]);
        } else if (userPreferences[1].includes(users[user])) {
            userLog.push(users[user]);
            notificationTokens.push(tokens[user]);
        }
    }
    return notificationTokens;
}

function getTitle(deviceType, displayName, deviceStatus) {
    let result = "";
    switch (deviceType) {
        case "doorContact":
            result += "Puerta abierta";
            break;
        case "battery":
            result += "Batería baja";
            break;
        case "windowContact":
            result += "Ventana manual abierta";
            break;
        case "window":
            result += "Ventana automática abierta";
            break;
        case "colorLight":
        case "switchLight":
        case "adjustableLight":
            result += "Luz encendida";
            break;
        case "garages":
            result += "Portón abierto";
            break;
        case "lockDoors":
            result += "Puerta abierta";
            break;
        case "thermostat":
            result += `Cambio de configuración: ${deviceStatus}`;
            break;
        case "curtain":
            result += "Cortina abierta";
            break;
        case "motion":
            result += "Movimiento detectado";
            break;
        default:
            result += displayName;

    }
    return result;
}